FROM python:3.8-slim

# install requirements
COPY requirements.txt /usr/bin
WORKDIR /usr/bin
RUN pip install -r requirements.txt

# copy the pipe source code
COPY pipe /usr/bin/
COPY pipe.yml /usr/bin

ENTRYPOINT ["python3", "/usr/bin/main.py"]
