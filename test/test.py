import boto3
import logging
import requests
import socket

from bitbucket_pipes_toolkit.test import PipeTestCase

logger = logging.getLogger(__name__)


class AwsEcsTaskDeployTestCase(PipeTestCase):

    def setUp(self):
        requests.post(f'{self.aws_endpoint_url()}/moto-api/reset')
        self.ecs = boto3.client(
            'ecs',
            region_name='eu-west-1',
            endpoint_url=self.aws_endpoint_url(),
            aws_access_key_id='foobar',
            aws_secret_access_key='barfoo'
        )

    def get_ip(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect(('10.255.255.255', 1))
            ip = s.getsockname()[0]
        except:
            ip = '127.0.0.1'
        finally:
            s.close()
        return ip

    def aws_endpoint_url(self):
        return f'http://{self.get_ip()}:5000'

    def get_aws_endpoint_data(self, service='ecs'):
        return requests.get(f'{self.aws_endpoint_url()}/moto-api/data.json').json()[service]

    def run_container(self, *args, **kwargs):
        aws_endpoint_url = self.aws_endpoint_url()

        logger.info(f'Using AWS ECS up on {aws_endpoint_url}')

        kwargs['environment'].update({
            'AWS_ENDPOINT_URL': aws_endpoint_url
        })
        return super().run_container(*args, **kwargs)

    def test_update_task_definition(self):
        variables = """
            {
                'my_variable': 'my value',
                'awslogs_group': 'log-group',
                'awslogs_region': 'eu-west-1',
                'awslogs_stream_prefix': 'b1'
            }
            """

        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': 'key-id',
            'AWS_SECRET_ACCESS_KEY': 'key-secret',
            'AWS_DEFAULT_REGION': 'eu-west-1',
            'TASK_DEFINITION': 'test/task-definition.json',
            'TASK_FAMILY': 'my-task',
            'TASK_ROLE_ARN': 'arn:aws:iam::1234567890:role/taskRole',
            'NETWORK_MODE': 'bridge',
            'REPLACEMENT_VARIABLES': variables,
            'DEBUG': True
        })

        self.assertRegex(result, r'Successfully deployed task revision for my-task.')
        self.assertNotRegex(result, r'Successfully updated service')
        self.assertRegex(result, r'Pipe finished.')

        moto_data = self.get_aws_endpoint_data()

        self.assertEqual(len(moto_data['Service']), 0)
        self.assertEqual(len(moto_data['Cluster']), 0)
        self.assertEqual(len(moto_data['TaskDefinition']), 1)

        self.assert_task_definition(moto_data['TaskDefinition'][0]['response_object'])

    def test_update_task_definition_capitalize_replacement(self):
        variables = """
            {
                'environment': 'staging',
                'awslogs_group': 'log-group',
                'awslogs_region': 'eu-west-1',
                'awslogs_stream_prefix': 'b1'
            }
            """

        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': 'key-id',
            'AWS_SECRET_ACCESS_KEY': 'key-secret',
            'AWS_DEFAULT_REGION': 'eu-west-1',
            'TASK_DEFINITION': 'test/task-definition-capitalize-replacement.json',
            'TASK_FAMILY': 'capitalizing-task',
            'TASK_ROLE_ARN': 'arn:aws:iam::1234567890:role/taskRole',
            'NETWORK_MODE': 'bridge',
            'REPLACEMENT_VARIABLES': variables,
            'DEBUG': True
        })

        self.assertRegex(result, r'Successfully deployed task revision for capitalizing-task.')
        self.assertNotRegex(result, r'Successfully updated service')
        self.assertRegex(result, r'Pipe finished.')

        moto_data = self.get_aws_endpoint_data()

        self.assertEqual(len(moto_data['Service']), 0)
        self.assertEqual(len(moto_data['Cluster']), 0)
        self.assertEqual(len(moto_data['TaskDefinition']), 1)

        task_definition = moto_data['TaskDefinition'][0]['response_object']
        self.assertEqual(task_definition['family'], 'capitalizing-task')
        self.assertEqual(task_definition['revision'], 1)
        self.assertEqual(task_definition['taskDefinitionArn'], f'arn:aws:ecs:eu-west-1:012345678910:task-definition/capitalizing-task:1')
        self.assertEqual(len(task_definition['volumes']), 0)
        self.assertEqual(len(task_definition['containerDefinitions']), 1)
        container_definition = task_definition['containerDefinitions'][0]
        self.assertEqual(container_definition['name'], 'task-image')
        self.assertEqual(container_definition['image'], 'task-image:latest')
        self.assertEqual(container_definition['memory'], 2048)
        self.assertTrue(container_definition['essential'])
        self.assertEqual(len(container_definition['portMappings']), 1)
        port_mapping = container_definition['portMappings'][0]
        self.assertEqual(port_mapping['hostPort'], 8080)
        self.assertEqual(port_mapping['protocol'], 'tcp')
        self.assertEqual(port_mapping['containerPort'], 8080)
        self.assertEqual(len(container_definition['environment']), 2)
        environment = container_definition['environment'][0]
        self.assertEqual(environment['name'], 'ENVIRONMENT')
        self.assertEqual(environment['value'], 'Staging')
        env_name = container_definition['environment'][1]
        self.assertEqual(env_name['name'], 'ENV_NAME')
        self.assertEqual(env_name['value'], 'staging')
        self.assertIsNotNone(container_definition['logConfiguration'])
        self.assertEqual(container_definition['logConfiguration']['logDriver'], 'awslogs')
        self.assertIsNotNone(container_definition['logConfiguration']['options'])
        self.assertEqual(container_definition['logConfiguration']['options']['awslogs-group'], 'log-group')
        self.assertEqual(container_definition['logConfiguration']['options']['awslogs-region'], 'eu-west-1')
        self.assertEqual(container_definition['logConfiguration']['options']['awslogs-stream-prefix'], 'b1')

    def test_update_task_definition_and_service(self):
        self.ecs.create_cluster(clusterName='my-cluster')
        self.ecs.register_task_definition(
            family='my-task',
            containerDefinitions=[{
                'name': 'task-image',
                'image': 'task-image:latest',
                'cpu': 1024,
                'memory': 400,
                'essential': True,
                'environment': [{
                    'name': 'AWS_ACCESS_KEY_ID',
                    'value': 'SOME_ACCESS_KEY'
                }],
                'logConfiguration': {
                    'logDriver': 'json-file'
                }
            }]
        )
        self.ecs.create_service(
            cluster='my-cluster',
            serviceName='my-service',
            taskDefinition='my-task',
            desiredCount=2
        )

        variables = """
            {
                'my_variable': 'my value',
                'awslogs_group': 'log-group',
                'awslogs_region': 'eu-west-1',
                'awslogs_stream_prefix': 'b1'
            }
            """

        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': 'key-id',
            'AWS_SECRET_ACCESS_KEY': 'key-secret',
            'AWS_DEFAULT_REGION': 'eu-west-1',
            'TASK_DEFINITION': 'test/task-definition.json',
            'TASK_FAMILY': 'my-task',
            'TASK_ROLE_ARN': 'arn:aws:iam::1234567890:role/taskRole',
            'NETWORK_MODE': 'bridge',
            'REPLACEMENT_VARIABLES': variables,
            'CLUSTER_NAME': 'my-cluster',
            'SERVICE_NAME': 'my-service',
            'DEBUG': True
        })

        self.assertRegex(result, r'Successfully deployed task revision for my-task.')
        self.assertRegex(result, r'Successfully updated service my-service on my-cluster.')
        self.assertRegex(result, r'Pipe finished.')

        moto_data = self.get_aws_endpoint_data()

        self.assertEqual(len(moto_data['Service']), 1)
        self.assertEqual(len(moto_data['Cluster']), 1)
        self.assertEqual(len(moto_data['TaskDefinition']), 2)

        self.assert_task_definition(moto_data['TaskDefinition'][1]['response_object'], revision=2)

    def test_update_task_definition_and_unknown_service(self):
        self.ecs.create_cluster(clusterName='my-cluster')

        variables = """
            {
                'my_variable': 'my value',
                'awslogs_group': 'log-group',
                'awslogs_region': 'eu-west-1',
                'awslogs_stream_prefix': 'b1'
            }
            """

        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': 'key-id',
            'AWS_SECRET_ACCESS_KEY': 'key-secret',
            'AWS_DEFAULT_REGION': 'eu-west-1',
            'TASK_DEFINITION': 'test/task-definition.json',
            'TASK_FAMILY': 'my-task',
            'TASK_ROLE_ARN': 'arn:aws:iam::1234567890:role/taskRole',
            'NETWORK_MODE': 'bridge',
            'REPLACEMENT_VARIABLES': variables,
            'CLUSTER_NAME': 'my-cluster',
            'SERVICE_NAME': 'my-service',
            'DEBUG': True
        })

        self.assertRegex(result, r'Successfully deployed task revision for my-task.')
        self.assertNotRegex(result, r'Successfully updated service')
        self.assertRegex(result, r'ECS service not found. Check your SERVICE_NAME.')
        self.assertNotRegex(result, r'Pipe finished.')

        moto_data = self.get_aws_endpoint_data()

        self.assertEqual(len(moto_data['Service']), 0)
        self.assertEqual(len(moto_data['Cluster']), 1)
        self.assertEqual(len(moto_data['TaskDefinition']), 1)

        self.assert_task_definition(moto_data['TaskDefinition'][0]['response_object'])

    def assert_task_definition(self, task_definition, revision=1):
        self.assertEqual(task_definition['family'], 'my-task')
        self.assertEqual(task_definition['revision'], revision)
        self.assertEqual(task_definition['taskDefinitionArn'], f'arn:aws:ecs:eu-west-1:012345678910:task-definition/my-task:{revision}')
        self.assertEqual(len(task_definition['volumes']), 0)
        self.assertEqual(len(task_definition['containerDefinitions']), 1)
        container_definition = task_definition['containerDefinitions'][0]
        self.assertEqual(container_definition['name'], 'task-image')
        self.assertEqual(container_definition['image'], 'task-image:latest')
        self.assertEqual(container_definition['memory'], 2048)
        self.assertTrue(container_definition['essential'])
        self.assertEqual(len(container_definition['portMappings']), 1)
        port_mapping = container_definition['portMappings'][0]
        self.assertEqual(port_mapping['hostPort'], 8080)
        self.assertEqual(port_mapping['protocol'], 'tcp')
        self.assertEqual(port_mapping['containerPort'], 8080)
        self.assertEqual(len(container_definition['environment']), 1)
        environment = container_definition['environment'][0]
        self.assertEqual(environment['name'], 'MY_VARIABLE')
        self.assertEqual(environment['value'], 'my value')
        self.assertIsNotNone(container_definition['logConfiguration'])
        self.assertEqual(container_definition['logConfiguration']['logDriver'], 'awslogs')
        self.assertIsNotNone(container_definition['logConfiguration']['options'])
        self.assertEqual(container_definition['logConfiguration']['options']['awslogs-group'], 'log-group')
        self.assertEqual(container_definition['logConfiguration']['options']['awslogs-region'], 'eu-west-1')
        self.assertEqual(container_definition['logConfiguration']['options']['awslogs-stream-prefix'], 'b1')
