import boto3
import json
import re
import os
import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger
from botocore.exceptions import ClientError, ParamValidationError
from pprint import pformat

logger = get_logger()

schema = {
    'AWS_ACCESS_KEY_ID': {
        'type': 'string',
        'required': True
    },
    'AWS_SECRET_ACCESS_KEY': {
        'type': 'string',
        'required': True
    },
    'AWS_DEFAULT_REGION': {
        'type': 'string',
        'required': True
    },
    'TASK_DEFINITION': {
        'type': 'string',
        'required': True
    },
    'TASK_FAMILY': {
        'type': 'string',
        'required': False,
        'nullable': True,
        'default': None
    },
    'TASK_ROLE_ARN': {
        'type': 'string',
        'required': False,
        'nullable': True,
        'default': None
    },
    'NETWORK_MODE': {
        'type': 'string',
        'allowed': ['bridge', 'host', 'awsvpc', 'none'],
        'default': 'bridge'
    },
    'REPLACEMENT_VARIABLES': {
        'required': False,
        'nullable': True,
        'default': None
    },
    'CLUSTER_NAME': {
        'type': 'string',
        'required': False,
        'nullable': True,
        'default': None
    },
    'SERVICE_NAME': {
        'type': 'string',
        'required': False,
        'nullable': True,
        'default': None
    },
    'SCALING_ROLE_ARN': {
        'type': 'string',
        'required': False,
        'nullable': True,
        'default': None
    },
    'SERVICE_SCALING_DEFINITION': {
        'type': 'string',
        'required': False,
        'nullable': True,
        'default': None
    },
    'DEBUG': {
        'default': False,
        'type': 'boolean'
    }
}


class AwsEcsTaskDeploy(Pipe):

    def run(self):
        super().run()

        task_definition_file = self.get_variable('TASK_DEFINITION')
        task_definition = self.update_task_definition(task_definition_file)
        logger.debug(f'Registered task definition:\n{pformat(task_definition)}')

        task_family = task_definition['family']
        self.success(f'Successfully deployed task revision for {task_family}.')

        cluster_name = self.get_variable('CLUSTER_NAME')
        service_name = self.get_variable('SERVICE_NAME')

        if cluster_name is not None and service_name is not None:
            service_scaling_definition_file = self.get_variable('SERVICE_SCALING_DEFINITION')
            service_definition = self.update_service(cluster_name, service_name, task_definition, service_scaling_definition_file)
            logger.debug(f'Updated service definition:\n{pformat(service_definition)}')
            self.success(f'Successfully updated service {service_name} on {cluster_name}.')

        self.success('Pipe finished.')

    def update_task_definition(self, task_definition_file):
        logger.info(f'Reading task definition...')
        ecs = self.get_ecs()

        with open(task_definition_file, 'r') as file:
            task_definition = self.replace_variables(file.read())

        logger.info('Updating task definition {0}...'.format(task_definition['family']))
        logger.debug(f'Registering task definition:\n{pformat(task_definition)}')
        try:
            response = ecs.register_task_definition(**task_definition)
            return response['taskDefinition']
        except ClientError as err:
            self.fail('Failed to update the task definition.\n' + str(err))
        except ParamValidationError as err:
            self.fail(f'ECS task definition parameter validation error:\n{err.args[0]}')

    def update_service(self, cluster_name, service_name, task_definition, service_scaling_definition_file):
        logger.info(f'Updating service {service_name}...')

        ecs = self.get_ecs()

        try:
            response = ecs.update_service(
                cluster=cluster_name,
                service=service_name,
                taskDefinition=task_definition['taskDefinitionArn'],
            )
            service_definition = response['service']

            if service_scaling_definition_file is not None:
                logger.info(f'Updating service auto scaling for {service_name}...')

                application_autoscaling = self.get_application_autoscaling()

                with open(service_scaling_definition_file, 'r') as file:
                    service_scaling_definition = self.replace_variables(file.read())

                target = service_scaling_definition['target']
                self.add_autoscaling_variables(cluster_name, service_name, target)
                logger.info(f'Registering scalable target {service_name} on {cluster_name}')
                application_autoscaling.register_scalable_target(**target)

                scaling_policies = {}
                for policy in service_scaling_definition['policies']:
                    self.add_autoscaling_variables(cluster_name, service_name, policy)
                    logger.info('Adding scaling policy {0}'.format(policy['PolicyName']))
                    response = application_autoscaling.put_scaling_policy(**policy)
                    scaling_policies[policy['PolicyName']] = response['PolicyARN']

                alarm = service_scaling_definition['alarm']
                for action_name in ['OKActions', 'AlarmActions', 'InsufficientDataActions']:
                    if action_name in alarm:
                        alarm[action_name] = [scaling_policies[name] for name in alarm[action_name]]
                logger.info('Adding metric alarm {0}'.format(alarm['AlarmName']))
                self.get_cloudwatch().put_metric_alarm(**alarm)

            return service_definition
        except ClientError as err:
            self.handle_update_service_error(err)

    def add_autoscaling_variables(self, cluster_name, service_name, request):
        request['ServiceNamespace'] = 'ecs'
        request['ScalableDimension'] = 'ecs:service:DesiredCount'
        request['ResourceId'] = f'service/{cluster_name}/{service_name}'

    def replace_variables(self, definition):
        replacement_variables = json.loads(json.dumps(self.get_variable('REPLACEMENT_VARIABLES')))

        def replace(match):
            variable = match.group(1)

            value = self.get_variable(variable.upper())

            if value is None:
                value = replacement_variables.get(variable)

            if value is None:
                for key in [variable.lower(), variable.upper()]:
                    value = replacement_variables.get(key)
                    if value is not None:
                        if variable == variable.capitalize():
                            value = value.capitalize()
                        break

            if value is None:
                value = os.getenv(variable.upper())

            logger.debug(f'Replacing variable {variable} with {value}')
            if value is None:
                self.fail(f'Missing variable {variable} in pipe configuration. Check your REPLACEMENT_VARIABLES.')

            return value

        processed_definition = re.sub(r'{{(\w+)}}', replace, definition)
        return json.loads(processed_definition)

    def get_ecs(self):
        return self.get_client('ecs')

    def get_application_autoscaling(self):
        return self.get_client('application-autoscaling')

    def get_cloudwatch(self):
        return self.get_client('cloudwatch')

    def get_client(self, service):
        try:
            return boto3.client(service, region_name=self.get_variable('AWS_DEFAULT_REGION'), endpoint_url=os.getenv('AWS_ENDPOINT_URL'))
        except ClientError as err:
            self.fail(f'Failed to create boto3 {service} client.\n' + str(err))

    def handle_update_service_error(self, error):
        error_code = error.response['Error']['Code']
        if error_code == 'ClusterNotFoundException':
            msg = 'ECS cluster not found. Check your CLUSTER_NAME.'
        elif error_code == 'ServiceNotFoundException':
            msg = 'ECS service not found. Check your SERVICE_NAME.'
        else:
            msg = 'Failed to update the ECS service.\n' + str(error)
        self.fail(msg)


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = AwsEcsTaskDeploy(
        pipe_metadata=metadata,
        schema=schema,
        check_for_newer_version=False
    )
    pipe.run()
