# Bitbucket Pipelines Pipe: aws-ecs-task-deploy

Deploy a new revision of a task on AWS ECS, and is set, also update the service its assotiated with.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: marvelution/aws-ecs-task-deploy:0.0.3
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    TASK_DEFINITION: '<string>'
    TASK_FAMILY: '<string>' # Optional.
    TASK_ROLE_ARN: '<string>' # Optional.
    NETWORK_MODE: '<string>' # Optional.
    REPLACEMENT_VARIABLES: <json> # Optional.
    CLUSTER_NAME: '<string>' # Optional.
    SERVICE_NAME: '<string>' # Optional.
    SCALING_ROLE_ARN: '<string>' # Optional.
    SERVICE_SCALING_DEFINITION: <json> # Optional.
    DEBUG: <boolean> # Optional.
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**) | AWS access key id. |
| AWS_SECRET_ACCESS_KEY (**) | AWS access key secret. |
| AWS_DEFAULT_REGION (**) | AWS Rregion  |
| TASK_DEFINITION (*) | Path to the task definition json file. The file should contain a task definition as described in the [AWS docs][register_task_definition]. |
| TASK_FAMILY | Task family name, useful for variable replacement in the TASK_DEFINITION file. |
| TASK_ROLE_ARN | Task role resource arn, useful for variable replacement in the TASK_DEFINITION file. |
| NETWORK_MODE | Task network mode, useful for variable replacement in the TASK_DEFINITION file. |
| REPLACEMENT_VARIABLES | Variables used for replacement in the TASK_DEFINITION or SERVICE_SCALING_DEFINITION files. |
| CLUSTER_NAME | The name of your ECS cluster, only required when updating a service associated with the task. |
| SERVICE_NAME | The name of your ECS service, only required when updating a service associated with the task. |
| SCALING_ROLE_ARN| Auto scaling role resource arn, useful for variable replacement in the SERVICE_SCALING_DEFINITION file. |
| SERVICE_SCALING_DEFINITION | Path to the scaling definition json file. The file should contain a definition as described below. |
| DEBUG | Turn on extra debug information. |

_(*) = required variable._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to
 be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._

## Details

This pipe registers a new task definition revision, optionally replacing variables in the TASK_DEFINITION file with values provided to the
pipe configuration. Variable replacement works by replacing text matching regex `{{(\w+)}}` with the associated value in the following
order:

1. Check if the variable name references a pipe configuation variable,
2. Otherwise check if the variable name references a variable in the REPLACEMENT_VARIABLES json,
3. Lastly check if the variable name references an environment variable.

The pipe will fail if it unable to find a value for a variable.

## Prerequisites

Registering a new task definition revision doesn't have any prerequisites other then havng an AWS account setup.

To use this pipe to update a service you should have an ECS cluster running a service.
Learn how to create one [here](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_GetStarted_EC2.html).

## Service Scaling Definition

The service scaling definition file contains teh different elements needed to enable application auto scaling for the ECS service, and
looks like this:

```json
{
  "target": {},
  "policies": [],
  "alarm": {}
}
```

Where
* `target` references the definition that is used to [register a scalable target][register_scalable_target]
* `policies` references the defitions of policies used as actions of the alarm that triggers the scaling, see [put scaling policy][put_scaling_policy] documentation.
* `alarm` references the definition of the CloudWatch alarm where the alarm actions (OKActions, AlarmActions, InsufficientDataActions)
   reference the policies defined in the `policies` element by name, see [put_metric_alarm][put_metric_alarm].

For [registering the scalable target][register_scalable_target] as well as [adding the scaling policy][put_scaling_policy] the
`ServiceNamespace` is fixed to `ecs` and `ScalableDimension` `ecs:service:DesiredCount`. `ResourceId` is generated from configuration
provided to the pipe.

## Examples

Basic example:

```yaml
script:
  - pipe: marvelution/aws-ecs-task-deploy:0.0.3
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      TASK_DEFINITION: 'target/task-defintion.json'
```

Example with variables for replacement:

```yaml
script:
  - pipe: marvelution/aws-ecs-task-deploy:0.0.3
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      TASK_DEFINITION: 'target/task-defintion.json'
      TASK_FAMILY: 'my-task'
      TASK_ROLE_ARN: 'arn:aws:iam::1234567890:role/taskRole'
      NETWORK_MODE: 'bridge'
      REPLACEMENT_VARIABLES: >
        {
          'awslogs_region': '$AWS_DEFAULT_REGION',
          'awslogs_group': '/my/task'
          'awslogs_stream_prefix': 'b1'
        }
      DEBUG: True
```

Example with variables for updating a service:

```yaml
script:
  - pipe: marvelution/aws-ecs-task-deploy:0.0.3
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      TASK_DEFINITION: 'target/task-defintion.json'
      TASK_FAMILY: 'my-task'
      TASK_ROLE_ARN: 'arn:aws:iam::1234567890:role/taskRole'
      NETWORK_MODE: 'bridge'
      REPLACEMENT_VARIABLES: >
        {
          'awslogs_region': '$AWS_DEFAULT_REGION',
          'awslogs_group': '/my/task'
          'awslogs_stream_prefix': 'b1'
        }
      CLUSTER_NAME: 'my-cluster'
      SERVICE_NAME: 'my-service'
      SERVICE_SCALING_DEFINITION: 'target/scaling-defintion.json'
      DEBUG: True
```

Example service scaling definition:

```json
{
	"target": {
		"MinCapacity": 0,
		"MaxCapacity": 10,
		"RoleARN": "arn:aws:iam::1234567890:role/scalingRole"
	},
	"policies": [
		{
			"PolicyName": "WorkerScaleDownPolicy",
			"PolicyType": "StepScaling",
			"StepScalingPolicyConfiguration": {
				"AdjustmentType": "ChangeInCapacity",
				"StepAdjustments": [
					{
						"MetricIntervalLowerBound": -5.0,
						"MetricIntervalUpperBound": -3.0,
						"ScalingAdjustment": -1
					},
					{
						"MetricIntervalUpperBound": -5.0,
						"ScalingAdjustment": -1
					}
				],
				"MetricAggregationType": "Average",
				"Cooldown": 300
			}
		},
		{
			"PolicyName": "WorkerScaleUpPolicy",
			"PolicyType": "StepScaling",
			"StepScalingPolicyConfiguration": {
				"AdjustmentType": "ChangeInCapacity",
				"StepAdjustments": [
					{
						"MetricIntervalLowerBound": 0.0,
						"MetricIntervalUpperBound": 5.0,
						"ScalingAdjustment": 1
					},
					{
						"MetricIntervalLowerBound": 5.0,
						"MetricIntervalUpperBound": 10.0,
						"ScalingAdjustment": 2
					},
					{
						"MetricIntervalLowerBound": 10.0,
						"ScalingAdjustment": 3
					}
				],
				"MetricAggregationType": "Average",
				"Cooldown": 300
			}
		}
	],
	"alarm": {
		"AlarmName": "MyQueue-PendingMessages",
		"AlarmDescription": "There are too many messages pending in the queue.",
		"ActionsEnabled": true,
		"OKActions": [
			"WorkerScaleDownPolicy"
		],
		"AlarmActions": [
			"WorkerScaleUpPolicy"
		],
		"MetricName": "ApproximateNumberOfMessagesVisible",
		"Namespace": "AWS/SQS",
		"Statistic": "Maximum",
		"Dimensions": [
			{
				"Name": "QueueName",
				"Value": "MyQueue"
			}
		],
		"Period": 60,
		"EvaluationPeriods": 5,
		"Threshold": 5.0,
		"ComparisonOperator": "GreaterThanOrEqualToThreshold"
	}
}

```

## Support

If you'd like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License

Copyright (c) 2019 Marvelution.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[register_task_definition]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs.html#ECS.Client.register_task_definition
[register_scalable_target]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/application-autoscaling.html#ApplicationAutoScaling.Client.register_scalable_target
[put_scaling_policy]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/application-autoscaling.html#ApplicationAutoScaling.Client.put_scaling_policy
[put_metric_alarm]: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudwatch.html#CloudWatch.Client.put_metric_alarm
[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,jira
