# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.3

- patch: Bugfix in auto scaling alarm registration

## 0.0.2

- patch: Optionaly Capitalize Variable Replacement
- patch: Support enabling auto scaling for a service

## 0.0.1

- patch: Initial Release

